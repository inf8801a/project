
# Music genre recognition using spectrograms and visual descriptors
INF8801A project, Nathan Boisneault, Polytechnique Montréal.

## Original method
Costa Y., Oliveira L., Koerich A., Gouyon F. (2013) "Music Genre Recognition Using Gabor Filters and LPQ Texture Descriptors." In: Ruiz-Shulcloper J., Sanniti di Baja G. (eds) *Progress in Pattern Recognition, Image Analysis, Computer Vision, and Applications*. CIARP 2013. Lecture Notes in Computer Science, vol 8259. Springer, Berlin, Heidelberg. https://doi.org/10.1007/978-3-642-41827-3_9

## Presentation
This project is composed of three main components: the pipeline component for calculating audio files's descriptors, the training component for building a classification model and the testing component for predicting muscial genre based on the previously built classification model.

### Pipeline
For training as for testing, audio files are processed by going through a pipeline that does the following:
- Read the audio file. Any format that is supported by MATLAB (as MP3 or WAV) can be used.
- Split the signal into three 6-second segments corresponding to the beginning, the middle and the end of the song. The original method uses 10-second segments but the dataset used in this project does not come with audio files that are long enough.
- Produce a spectrogram for the resulting 18-second sample.
- Split the spectrogram into *n* zones. The default is 5.
- Calculate visual descriptors for each one of the 3*n* zones.
The code for the Pipeline class is in Pipeline.m.

### Train
The training component takes audio files in directories corresponding to different genres, calculate descriptors by passing the audio files through the pipeline, and calculate a classification model. Classification is made using support vector machines (SVMs) and non-linear, multiclass models. There are 3*n* models corresponding to the 3*n* zones the spectrograms are split into.
The code for the training component can be found in *train.m*.

### Test
The testing component takes audio files whose musical genres are unknown to the SVMs, calculate descriptors by passing the audio files through the pipeline, and try to predict the genre of each of these songs based on their descriptors and using the classification model produced during the training step. For a given audio file, each of the 3*n* SVMs corresponding to the 3*n* zones of the signal's spectrogram makes a prediction. The final prediction is the one that was most voted for by the SVMs.
The code for the testing component can be fount in *test.m*.

## Dataset
The data used for training and testing comes from the [Extended Ballroom dataset](http://anasynth.ircam.fr/home/media/ExtendedBallroom/). It contains 4,180 musics over 13 different genres. The audio file are approximately 30 seconds long. The songs are listed in the two XML files. Use the Python script to retrieve data. Music files are downloaded and stored by genre. See the data README for more info.

## How to use
1. Download the training and testing data using the Python script. Separate the training and the testing set. Be sure to keep files in directories corresponding to their musical genre. Ensure that there is no common file between the two sets.
2. Run *train.m* after setting the correct data path in the parameters. The classification models are saved in *model.mat* at the root of your data directory (alongside the directories for the different genres).
3. One the training is complete, run *test.m* after setting the correct data and test data paths. 