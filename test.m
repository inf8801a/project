%% Music genre recognition using spectrograms and visual descriptors
%  Nathan Boisneault, 2032187
%  Automne 2020
%  Polytechnique Montréal
%
%  Original method by:
%  Y. Costa, L. Oliveira, A. Koerich, and F. Gouyon, "Music Genre 
%  Recognition Using Gabor Filters and LPQ Texture Descriptors,"
%  doi: https://doi.org/10.1007/978-3-642-41827-3_9.
%
%% test.m - Try to guess musical genre with trained SVMs



%% Parameters

% List of studied genres
genres = {'Chacha', 'Tango', 'Waltz'};

% Location of classification models
dataDir = 'C:\TEMP\inf8801a\data\';

% Location of test audio files
testDir = 'C:\TEMP\inf8801a\test\';

% Descriptors database
testData = containers.Map;



%% Calculate descriptors for test data

% Show progression
h = waitbar(0, 'Calculating descriptors');

for i = 1:length(genres)
    genre = char(genres(i));
    dirList = dir([testDir genre]);

    for j = 1:length(dirList)
        file = dirList(j);

        if ~ file.isdir
            name = file.name;
            path = [testDir genre '\' name];
            
            % Show progression
            waitbar(j / length(dirList), h, [genre ': ' name]);

            % Calculate descriptor
            testData(name) = ...
                Pipeline(path, Pipeline.default_nzones, genre);
        end
    end
end

close(h);



%% Test SVMs with data

% Load models from training
load([dataDir 'models.mat']);

% Number of correct guesses
correct = 0;

keys = testData.keys;
n = Pipeline.default_nzones * 3;

% For each test file, try and predict genre
for i = 1:length(keys)
    pipeline = testData(keys{i});
    results = strings(n, 1);
    
    % Predict genre for each zones using dedicated SVMs
    for j = 1:n
        svm = models(j);
        results(j) = predict(svm, pipeline.descriptors(j, :));
    end
    
    % Take the value the most voted for by SVM as the predicted genre
    result = string(mode(categorical(results)));    
    if result == pipeline.genre, correct = correct + 1; end
end

% Calculate success rate
success = correct / length(keys);
