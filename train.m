%% Music genre recognition using spectrograms and visual descriptors
%  Nathan Boisneault, 2032187
%  Automne 2020
%  Polytechnique Montréal
%
%  Original method by:
%  Y. Costa, L. Oliveira, A. Koerich, and F. Gouyon, "Music Genre 
%  Recognition Using Gabor Filters and LPQ Texture Descriptors,"
%  doi: https://doi.org/10.1007/978-3-642-41827-3_9.
%
%% train.m - Read audio files, calculate descriptors and train SVMs

close all;
clear all;



%% Parameters

% List of studied genres
genres = {'Chacha', 'Tango', 'Waltz'};

% Location of genres directories containing audio files
dataDir = 'C:\TEMP\inf8801a\data\';

% Descriptors database
trainData = containers.Map;



%% Load data and calculate descriptors

h = waitbar(0, 'Calculating descriptors');

for i = 1:length(genres)
    genre = char(genres(i));
    dirList = dir([dataDir genre]);

    for j = 1:length(dirList)
        file = dirList(j);

        if ~ file.isdir
            name = file.name;
            path = [dataDir genre '\' name];

            % Show progression
            waitbar(j / length(dirList), h, [genre ': ' name]);

            % Calculate descriptors
            trainData(name) = ...
                Pipeline(path, Pipeline.default_nzones, genre);
        end
    end
end

close(h);



%% Train SVMs

keys = trainData.keys;
n = Pipeline.default_nzones * 3;
models = containers.Map('KeyType', 'double', 'ValueType', 'any');
try load([dataDir 'models.mat']), catch, end

descDim = size(Pipeline.scales, 1) * size(Pipeline.orientations, 1);
predictors = zeros(n, length(keys), descDim);
classes = strings(length(keys), 1);

% Descriptors aggregation by zone
for i = 1:length(keys)
    pipeline = trainData(keys{i});
    classes(i) = pipeline.genre;
    
    for j = 1:n
        predictors(j, i, :) = pipeline.descriptors(j, :);
    end
end

% Calculate and save models
for i = 1:n
    X = squeeze(predictors(i, :, :));
    models(i) = fitcecoc(X, classes);
    save([dataDir 'models.mat'], 'models');
end
