classdef Pipeline
% Calculate spectrogram and descriptors for the given audio file.

    properties (Constant)
        default_nzones = 5; % zones
        segsize = 6; % seconds
        trimsize = 5; % seconds
        default_rate = 22050; % Hertz

        % Gabor parameters
        scales = [32 16 8 4 2]; % 5 scale levels
        orientations = [0 22.5 45 67.5 90 112.5 135 157.5]; % 8 orientations
        zoneSize = [64 64];
        filters = gabor(Pipeline.scales, Pipeline.orientations);
    end

    properties
        filename;
        nzones;
        data;
        rate;
        duration;
        segments;
        spectrogram;
        zones;
        descriptors;
        genre;
    end

    methods

        function obj = Pipeline(filename, nzones, genre)
        % Constructor.
        % Creates and run a new processing pipeline for the provided audo
        % file.
        
            obj.filename = filename;
            obj.nzones = nzones;
            obj.genre = genre;
            
            obj = obj.readFile();
            obj = obj.split();
            obj = obj.createSpectrogram();
            obj = obj.splitSpectrogram();
            obj = obj.calculateDesc();
        end


        function obj = readFile(obj)
        % Read music from the audio file.

            % Read file
            [signal, Fs] = audioread(obj.filename);
            N = size(signal, 1);

            % Convert stereo to mono so as to have a one-channel signal
            signal = (signal(:, 1) + signal(:, 2)) .* .5;

            % Use a sample rate of 22.05kHz
            ratio = Pipeline.default_rate / Fs;
            n = N * ratio;
            points = linspace(1, N, n);
            signal = interp1((1:N), signal, points)';
            Fs = Pipeline.default_rate; % Hertz

            % Trim signal if long enough
            if size(signal, 1) > 3 * Pipeline.segsize * Fs
                n = Fs * Pipeline.trimsize;
                signal = signal(n + 1:size(signal, 1) - n);
            end

            obj.duration = size(signal, 1) / Fs;
            obj.rate = Fs;
            obj.data = signal;
        end


        function playAudio(obj)
        % Plays the file's audio signal.
            clear sound;
            sound(obj.data, obj.rate); 
        end

        function obj = split(obj)
        % Split signal in three segments.

            n = Pipeline.segsize * obj.rate;
            seg = zeros(3 * n, 1);

            % Beginning
            seg(1:n) = obj.data(1:n);

            % Middle
            mid = fix(size(obj.data, 1) / 2);
            first = mid - n / 2 + 1;
            last = mid + n / 2;
            seg(n + 1:2 * n) = obj.data(first:last);

            % End
            first = size(obj.data, 1) - n + 1;
            last = size(obj.data, 1);
            seg(2 * n + 1:3 * n) = obj.data(first:last);

            obj.segments = seg;
        end


        function obj = createSpectrogram(obj)
        % Build the signal's spectrogram.

            % Calculate power spectrum
            res = size(obj.segments, 1) / 256 / obj.rate;
            spect = pspectrum( ...
                obj.segments, ...
                obj.rate, ...
                "spectrogram", ...
                "TimeResolution", res);

            % Fix: remove null values (would be -Inf once in decibels
            spect(spect == 0) = min(spect(spect ~= 0));

            % Use decibel values and normalize to get a grayscale image
            spect = db(spect);
            spect = spect - min(spect(:));
            spect = spect ./ max(spect(:));

            obj.spectrogram = spect;
        end


        function showSpect(obj)
        % Display spectrogram
            imshow(obj.spectrogram);
        end


        function obj = splitSpectrogram(obj)
        % Split spectrogram in segments and zones.

            spect = obj.spectrogram;

            % Zones' dimensions
            h = fix(size(spect, 1) / obj.nzones);
            w = fix(size(spect, 2) / 3);

            % Split and display
            zon = zeros(h, w, 3 * obj.nzones);
            for i = 1:obj.nzones
                for j = 1:3
                    zindex = 3 * (i - 1) + j;
                    zhstart = (i - 1) * h + 1;
                    zhend = zhstart + h - 1;
                    zwstart = (j - 1) * w + 1;
                    zwend = zwstart + w - 1;
                    zon(:, :, zindex) = spect(zhstart:zhend, zwstart:zwend);
                end
            end

            obj.zones = zon;
        end


        function showZones(obj)
        % Display zones in new figure

            for i = 1:obj.nzones
                for j = 1:3
                    zindex = 3 * (i - 1) + j;
                    subplot(obj.nzones, 3, zindex), ...
                        imshow(obj.zones(:, :, zindex));
                end
            end
        end


        function obj = calculateDesc(obj)
        % Apply Gabor filters to each zone and calculate moments.
        % Calculated moments are mean, variance and skewness.

            % Apply filters to each subimage after scaling to zoneSize
            desc = zeros(size(obj.zones, 3), 120);

            for i = 1:size(obj.zones, 3)
                zone = imresize(obj.zones(:, :, i), Pipeline.zoneSize);
                filtered = imgaborfilt(zone, Pipeline.filters);

                % Calculate moments (mean, variance, skewness)
                for j = 1:40
                    index  = 3 * (j - 1) + 1;
                    desc(i, index) = mean(filtered(:, :, j), 'all');
                    desc(i, index + 1) = var(filtered(:, :, j), 1, 'all');
                    desc(i, index + 2) = skewness(filtered(:, :, j), 1, 'all');
                end
            end 

            obj.descriptors = desc;
        end
    end
end
